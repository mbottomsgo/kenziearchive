import os					# listdir
import json					# json dump
import re					# regex
import sys					# argv (command line arguments)
from datetime import date	# for naming backups
import tarfile				# zipping up the archive


# terminal printing colors!!!
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

## fns ##

# extracts db creds from file at file_loc
def get_wp_creds(file_loc):
	db_creds = ['name', 'user', 'pw']
	print('Fetching database credentials...')
	with open(file_loc) as content_file:
		content = content_file.read()
		content = content[content.index('DB_NAME'):content.index('hostname')]
		vitals = content.split("define")
		db_creds[0] = re.split('[\"\']', vitals[0])[2]
		db_creds[1] = re.split('[\"\']', vitals[1])[3]
		db_creds[2] = re.split('[\"\']', vitals[2])[3]
	print(bcolors.OKGREEN + 'Database credentials fetched.' + bcolors.ENDC)
	return db_creds
def get_drupal_creds(file_loc):
	db_creds = ['name', 'user', 'pw']
	print('Fetching database credentials...')
	with open(file_loc) as content_file:
		content = content_file.read()
		for line in content.split('\n'):
			if '*' not in line[0:3] and '#' not in line[0:3] and line != "":
				if "'database'" in line:
					db_creds[0] = re.sub('[\'\"\s,]', '', line).split('=>')[1]
				if "'username'" in line:
					db_creds[1] = re.sub('[\'\"\s,]', '', line).split('=>')[1]
				if "'password'" in line:
					db_creds[2] = re.sub('[\'\"\s,]', '', line).split('=>')[1]
	print(bcolors.OKGREEN + 'Database credentials fetched.' + bcolors.ENDC)
	return db_creds

# mysqldump
def dump_db(db_creds, parent_folder_path, dest_file_name):
	print('Dumping database...')
	os.popen('mysqldump -u %s -p%s -h localhost -e --opt -c %s > %s/.sql/%s.sql' % (db_creds[1], db_creds[2], db_creds[0], parent_folder_path, dest_file_name))
	print(bcolors.OKGREEN+'Database dumped to '+parent_folder_path+'/.sql/'+dest_file_name+'.sql'+bcolors.ENDC)

# zips up source/ into source.tar.gz
def tar_dir(source_dir):
	if '/public_html' in source_dir:
		source_dir = source_dir.replace('/public_html', '')
	print('Zipping to '+source_dir+'.tar.gz...')
	with tarfile.open(source_dir+'.tar.gz', "w:gz") as tar:
		tar.add(source_dir, arcname=os.path.basename(source_dir))
	print(bcolors.OKGREEN+'Zipped to '+source_dir+'.tar.gz'+bcolors.ENDC)

# gets .conf files in sites_available that match the domain
def get_sites(domain):
	vhosts = []
	for dir in os.listdir(sites_available):
		if domain in dir:
			vhosts.append(dir)
		elif domain[0:-4] in dir:
			if raw_input(dir+': Should this similar domain also be included? (y/n)' ) == 'y':
				vhosts.append(dir)
	return vhosts

# gets the DocumentRoot from the vhost_file
# TODO: work around vhost files that don't follow the naming conventions
def get_doc_root(vhost_file):
	path = ""
	with open(vhost_file, 'r') as content_file:
		content = content_file.read()
		start = content.index('DocumentRoot')+13
		end = content.index('\n', start)
		path = content[start:end]
	return path


## init ##

apache_dir = '/etc/apache2'
sites_available = apache_dir + '/sites-available'
sites_enabled = apache_dir + '/sites-enabled'

def backup(domain):
	domain_body = domain[0:-4]		# domain
	exempt = ['~', 'godigitaldev', 'godigitalmarketing']
	vhosts = []
	
	## action ##
	
	vhosts = get_sites(domain)
	
	if len(vhosts) == 0:
		print('No sites found.')
		return 'False'
	elif len(vhosts) < 0:
		# checks for conflicting DocumentRoots
		for i in range(1, len(vhosts)):
			if get_doc_root(vhosts[i]) != get_doc_root(vhosts[0]):
				print(bcolors.FAIL +'There are multiple conflicting virtualhost DocumentRoots. Please fix this and try again.'+bcolors.ENDC)
				return 'False'
	else:
		# finds DocumentRoot according to vhost
		document_root = get_doc_root(sites_available + '/' + vhosts[0])
		sitename = document_root.split('/')[-1]
	
		# finds db creds according to wp-config.php		
		if 'wp-config.php' in os.listdir(document_root):
			db_creds = get_wp_creds(document_root+'/wp-config.php')
		elif 'sites' in os.listdir(document_root):
			db_creds = get_drupal_creds(document_root+'/sites/default/settings.php')
		else:
			print(bcolors.FAIL +'This site is not WordPress or Drupal. You must back up this site manually.'+bcolors.ENDC)
			return 'False'
		
		#create document_root/.sql if it doesn't exist
		if not os.path.exists(document_root + '/.sql'):
			os.makedirs(document_root + '/.sql')
			print('./sql created in folder '+document_root)
		
		# mysqldump
		dump_db(db_creds, document_root, str(date.today()))
		
		# zipping up the archive
		tar_dir(document_root)
		
		return document_root