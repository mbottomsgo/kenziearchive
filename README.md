## Usage ##
From clients/morrison/kenziearchive:

	$ python archive.py domaintoarchive.com

or:

	$ python backup.py domaintobackup.com

### backup.py: ###

1.	Searches /etc/apache2/sites-available for a domain similar to the one specified and finds the document root associated with it

	Note:	If there is more than one, and if they don't all match, the script tells you so and stops

2.	Searches the document root for MySQL database credentials (username, password, database name)
	
	Note:	Works for WordPress and Drupal only

3.	If there is not a .sql folder in the document root, one is created.

4.	Dumps the database into DocumentRoot/.sql/YYYY-MM-DD.sql

5.	Zips the document root into DocumentRoot.tar.gz, unless the document root ends in "public_html", in which case it zips it into the document root's parent folder, which usually matches name of the site

### archive.py: ###

1.	Does everything in backup.py

2.	If backup.py finished successfully, DocumentRoot.tar.gz is moved to /home/ubuntu/archive.

	Note:	"sudo" may sometimes be necessary

3.	Disables the virtual host for the domain given

4.	Reloads Apache

5.	Recursively removes the original document root and its contents