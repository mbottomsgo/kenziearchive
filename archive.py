# usage:	python archive.py sitetobearchived.com

import kenziearchive	# to avoid redundancy
import sys				# command line args
import shutil			# moving .tar.gz to /ubuntu/archive
import os				# popen

domain = sys.argv[1]			# domain.com
document_root = kenziearchive.backup(domain)
if document_root == 'False':
	print(kenziearchive.bcolors.FAIL+'Failed.'+kenziearchive.bcolors.ENDC)
else:
	zip_location = document_root.replace('/public_html', '')
	sitename = zip_location.split('/')[-1]
	
	shutil.move(zip_location+'.tar.gz', '/home/ubuntu/archive/'+sitename+'.tar.gz')
	print(kenziearchive.bcolors.OKGREEN+'Moved '+sitename+'.tar.gz to /home/ubuntu/archive/'+kenziearchive.bcolors.ENDC)
	
	os.popen('a2dissite '+domain)
	print(kenziearchive.bcolors.OKGREEN+domain+' disabled.'+kenziearchive.bcolors.ENDC)
	os.popen('service apache2 reload')

	shutil.rmtree(document_root)
	print(kenziearchive.bcolors.OKBLUE+'Deleted '+document_root+kenziearchive.bcolors.ENDC)