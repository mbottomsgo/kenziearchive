# usage:	python setup-vhost.py
# run from the to-be DocumentRoot of the site you're setting up
# run right after unzipping

import kenziearchive	# staying dry
import os				# popen
from subprocess import call
import sys				# cmd line args
import shlex

def gen_conf_file(ip, domain, root):
	conf = '''<VirtualHost %:80>
  ServerName #
  RewriteEngine on
  RewriteRule ^/(.*) http://www.#/$1 [L,R=301,NC]
</VirtualHost>

<VirtualHost %:80>
  ServerAdmin webmaster@#
  ServerName www.#
  DocumentRoot &
  <Directory &>
  Require all granted
  AllowOverride ALL
  </Directory>
</VirtualHost>'''
	conf = conf.replace('#', domain)
	conf = conf.replace('&', doc_root)
	conf = conf.replace('%', ip)
	return conf

def write_conf_file(path, conf):
	conf_file = open(path, 'w')
	conf_file.write(conf)
	conf_file.close()
	print(kenziearchive.bcolors.OKGREEN+'Created '+domain+'.conf'+kenziearchive.bcolors.ENDC)

def place_conf_file(conf_path, conf):
	if os.path.exists(conf_path):
		confirm = raw_input(conf_path+' already exists. Do you want to overwrite it? (y/n) ')
		if confirm == 'y':
			write_conf_file(conf_path, conf)
		else:
			print(kenziearchive.bcolors.FAIL+'Please edit the existing conf file.'+kenziearchive.bcolors.ENDC)
	else:
		write_conf_file(conf_path, conf)

doc_root = os.getcwd()

confirm = 'n'
while (confirm != 'y'):
	domain = raw_input('Please enter the live domain of the site, whether or not you\'re going live yet. ')
	confirm = raw_input(domain+': Is that right? (y/n) ')

ip = raw_input('Please enter the IP address of this server: ')
conf = gen_conf_file(ip, domain, doc_root)
conf_path = kenziearchive.sites_available+'/'+domain+'.conf'

place_conf_file(conf_path, conf)