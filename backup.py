# usage:	python backup.py sitetobebackedup.com

import kenziearchive	# to avoid redundancy
import sys				# command line args

domain = sys.argv[1]			# domain.com
document_root = kenziearchive.backup(domain)
if document_root == 'False':
	print(kenziearchive.bcolors.FAIL+'Failed.'+kenziearchive.bcolors.ENDC)
else:
	print(kenziearchive.bcolors.OKBLUE+'Backed up '+domain+kenziearchive.bcolors.ENDC)