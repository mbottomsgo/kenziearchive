import os, os.path
import shutil			# removing dir 

search_pool = os.getcwd()

site_folders = []

for site_folder in os.listdir(search_pool):
	if os.path.isdir(site_folder):
		if os.path.exists(site_folder+"/public_html"):
			site_folders.append(site_folder + "/public_html")
		else:
			site_folders.append(site_folder)

wp_sites = []

for site_folder in site_folders:
	if os.path.exists(site_folder+"/wp-content/plugins"):
		wp_sites.append(site_folder)

plugins_in_question = ['nextgen-gallery', 'comprehensive-google-map-plugin', 'wp-site-migrate']

for site in wp_sites:
	for plugin in os.listdir(site+"/wp-content/plugins"):
		if plugin in plugins_in_question:
			if (plugin == 'wp-site-migrate'):
				shutil.rmtree(site+'/wp-content/plugins/wp-site-migrate')
			print plugin+" in "+site